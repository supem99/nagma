<?php  include ('include/header.php'); ?>
<?php  include ('include/navbar.php'); ?>

<!-- Header -->
<header id="header">
    <div class="intro">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 intro-text">

                       <font size="100" color="#f0f8ff"> <b>Home Construction<br>
                                 &<br>
                               Remodeling</b></font>
                       <h1><b><p> Welcome to Nagma Constructions (Pvt) Ltd</p></b></h1>

                </div>
            </div>
        </div>
    </div>
</header>

<?php

include "imghome.php";

?>



<!-- Services Section -->
<div id="services">
    <div class="container">
        <div class="section-title">
            <h2>Our Services</h2>
        </div>

        <div class="about-text">
            <p><b>Mainly we imported all the equipments from Europe countries but we can import them for customers choice.
                    We planning the house design and we give the 3D design of it to the customer. Also We make branded houses using new technology.</b>
            </p>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-1.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>New Home Construction</h3>

                </div>
            </div>
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-2.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>Home Additions</h3>

                </div>
            </div>
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-3.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>Bathroom Remodels</h3>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-4.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>Kitchen Remodels</h3>

                </div>
            </div>
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-5.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>Windows & Doors</h3>

                </div>
            </div>
            <div class="col-md-4">
                <div class="service-media"> <img src="http://cdn.home-designing.com/wp-content/uploads/2014/07/free-3-bedroom-house-plans.jpeg" alt=" "> </div>
                <div class="service-desc">
                    <h3>Planning</h3>

                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!--about-->

<div id="about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <img src="img/img.jpg" class="img-responsive" alt="">
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="about-text">
                    <h2>Who We Are</h2>
                    <p>

                    We are Nagma Constructions Private Limited. We construct and remodelling the houses.
                    Also, we decorate the gardens and doing all electric works and in houses etc. We have 25 years’
                    experience including 15 years in Europe and 10 years in Sri Lanka (London, Nederland, Paris, Spain, Belgium, Italy).
                    Also, we have men fir working in every sector in the construction series.
                    
                    </p>
                    <h3>Why Choose Us?</h3>
                    <div class="list-style">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <ul>
                                <li>Years of Experience</li>
                                <li>Fully Insured</li>
                                <li>Cost Control Experts</li>
                                <li>100% Satisfaction Guarantee</li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <ul>
                                <li>Free Consultation</li>
                                <li>Satisfied Customers</li>
                                <li>Project Management</li>
                                <li>Affordable Pricing</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/about-->


<!-- Contact Section -->

<!--last-->

<div id="contact">
    <div class="container">
        <div class="col-md-8">
            <div class="row">
                <div class="section-title">
                    <h2>Getting touch with us </h2>
                    <p>Please fill out the form below to send us an email and we will get back to you as soon as possible.</p>
                </div>

                <form novalidate action="contMail.php" method='post'>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Name " required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                        <input type="text" class="form-control" name="contNum" placeholder="Contact Number" maxlength="100" required>
                            </div>
                        </div>
                    </div>
                    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                        <input type="text" class="form-control" name="conMail" placeholder="Email" required>
                                    </div>
                                </div>
                            </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                        <input type="text" class="form-control" name="conSubj" placeholder="Subject" required>
                                            </div>
                                        </div>
                                    </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                        <textarea class="form-control" name="conMsg" rows="6"   placeholder="Your Message or Question." required></textarea>
                                                    </div>
                                                </div>
                                            </div>

                    <button class="btn btn-custom btn-lg" type="submit">Send</button>
                </form>
            </div>
        </div>



                <div class="col-md-3 col-md-offset-1 contact-info">
                    <div class="contact-item">
                        <h4>Contact Info</h4>
                        <p><span>Address</span>
                            No:583/ A Galle Road<br>
                            Wadduwa</p>
                    </div>
                    <div class="contact-item">
                        <p><span>Phone</span> +94 77 313 5839
                            <i class="fa fa-whatsapp"></i>
                        </p>
                        <p><span>Fax number</span> +94 38 22 94 025
                        </p>
                    </div>
                    <div class="contact-item">
                        <p><span>Email</span> construction.nagma@gmail.com</p>
                    </div>
                </div>
<!--last-->


<!--        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>-->
<!--        <script>-->
<!--            swal('ඔබට සිංහල පරිවර්තනය කිරීමට අවශ්‍ය නම් කරුණාකර "සිං" බොත්තම ක්ලික් කරන්න');-->
<!---->
<!--        </script>-->
<?php  include ('include/footer.php'); ?>
<?php  include ('include/script.php'); ?>

