<?php  include ('include/header.php'); ?>

<!-- Navigation==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="row">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand page-scroll" href="#page-top">NAGMA</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php" class="page-scroll">Home</a></li>
                    <li><a href="#portfolio" class="page-scroll">Our Works</a></li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </div>
</nav>




<div id="portfolio">
    <div class="container">
        <div class="section-title">
            <h2>Our Works</h2>
        </div>
        <div class="row">
            <div class="portfolio-items">

<!--       Head         -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/1.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/1.jpg" class="img-responsive" alt="Project "> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/2.jpg" title="New Home Construction" data-lightbox-gallery="gallery1">
                                <img src="img/new/1/2.jpg" class="img-responsive" alt="Project "> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/3.jpg" data-lightbox-gallery="gallery1">
                                <img src="img/new/1/3.jpg" class="img-responsive" alt="Project "> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/3.jpg"  data-lightbox-gallery="gallery1"4
                                <img src="img/new/1/3.jpg" class="img-responsive" alt="Project Title"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/4.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/4.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/5.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/5.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/6.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/6.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/7.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/7.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/wnew/1/8.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/8.jpg" width="500" height="220"  alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/10.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/10.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/11.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/11.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>


                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/12.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/12.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/13.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/13.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/14.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/14.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/15.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/15.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/16.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/16.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/17.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/17.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/18.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/18.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/19.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/19.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/20.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/20.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/21.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/21.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/22.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/22.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/23.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/23.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/24.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/24.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/25.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/25.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/31.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/31.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/26.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/26.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/27.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/27.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/28.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/28.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/29.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/29.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/30.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/30.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/32.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/32.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/33.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/33.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/34.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/34.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/35.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/35.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/36.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/36.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/37.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/37.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/38.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/38.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/39.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/1/39.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>




                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/2/1.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/2/1.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/2/2.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/2/2.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/2/3.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/2/3.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/2/5.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/2/5.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/2/6.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/2/6.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/2/7.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/2/7.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/2/4.jpg"  data-lightbox-gallery="gallery1">
                                <img src="img/new/2/4.jpg" class="img-responsive" alt="Project"> </a> </div>
                    </div>
                </div>

<!--Head End-->
            </div>
        </div>
    </div>
</div>



<div id="contact">
    <div class="container">
        <div class="col-md-12">
<?php  include ('include/footer.php'); ?>
<?php  include ('include/script.php'); ?>

