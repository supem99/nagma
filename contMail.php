<?php
header("Content-Type:text/html; charset=UTF-8");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

function setFilter($val) {
    $step1 = trim($val);
    $step2 = strip_tags($step1);
    $step3 = htmlspecialchars($step2, ENT_QUOTES);
    $result = $step3;
    return $result;
}

$pname = setFilter($_POST["name"]);
$contact = setFilter($_POST["contNum"]);
$email = setFilter($_POST["conMail"]);
$subject = setFilter($_POST["conSubj"]);
$message = setFilter($_POST["conMsg"]);




$_SESSION['sent_message'] = "";
$_SESSION['thnx_message'] = "";
$_SESSION['getBack_message'] = "";
$_SESSION['go_backBtn'] = "";


$_SESSION['eror'] = "";
$_SESSION['eror_message'] = "";
$_SESSION['retry_message'] = "";
$_SESSION['go_backBtn'] = "";







$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = 0;
    // (0): Disable debugging (you can also leave this out completely, 0 is the default).
    // (1): Output messages sent by the client.
    // (2): as 1, plus responses received from the server (this is the most useful setting).
    // (3): as 2, plus more information about the initial connection - this level can help diagnose STARTTLS failures.
    // (4): as 3, plus even lower-level information, very verbose, don't use for debugging SMTP, only low-level problems.

    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->CharSet = "utf-8";
    $mail->SMTPAuth = true;
    $mail->Username = 'customer.message00@gmail.com';
    $mail->Password = 'Chandula@Supem123';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;
    $mail->SMTPOptions = array(
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        ],
    );

    //Recipients
    $mail->setFrom('construction.nagma@gmail.com', 'Customer Mailer');
    $mail->addAddress('construction.nagma@gmail.com');
    $mail->addReplyTo($email, $pname);

    // Content
    $mail->isHTML(true);
    $mail->Subject = 'Customer Message';
    $mail->Body = "<h2 align=center>$subject</h2><br><br>
    Hello,<br/> I'm $pname.<br> 
    This is my contact number : $contact<br>
    This is my email : $email<br> 
    <br>
    $message";

    $mail->AltBody = "$subject<br><br>Hello,<br/> I'm $pname.<br> 
    This is my contact number : $contact<br> 
    <br>$message";

    $mail->send();
    //echo 'Message has been sent';

    $_SESSION['sent_message'] = "<h1 style='color: rgb(39, 231, 21);'>Message has been sent</h1>";
    $_SESSION['thnx_message'] = "<h3>Thanks $pname for connecting with us.</h3>";
    $_SESSION['getBack_message'] = "<h4>We'll get back to you soon.</h4>";
    $_SESSION['go_backBtn'] = "<a class='btn btn-info' href='index.php'>Go Back</a>";
} catch (Exception $e)
{
    //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";

    $_SESSION['eror'] = "<h1 style='color: rgb(226, 5, 5);'>Ooops ! Something went wrong.</h1>";
    $_SESSION['eror_message'] = "<h2>Message could not be sent.</h2>";
    $_SESSION['retry_message'] = "<h4>Try again later.</h4>";
    $_SESSION['go_backBtn'] = "<a class='btn btn-custom btn-lg' href='index.php'>Go Back</a>";
}
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <title>Contact Us</title>
</head>

<body>

    <div class="container-fluid">

        <div class="container">
            <?php
                if ($_SESSION['sent_message'] && $_SESSION['thnx_message'] && $_SESSION['getBack_message'] && $_SESSION['go_backBtn'] != null) {
                    echo "<div class='container' style='margin-top: 50px;'>";
                    echo "<div class='row mb-5 justify-content-center align-items-center'>" . $_SESSION['sent_message'] . "</div>";
                    echo "<div class='row mb-2 justify-content-center align-items-center'>" . $_SESSION['thnx_message'] . "</div>";
                    echo "<div class='row mb-3 justify-content-center align-items-center'>" . $_SESSION['getBack_message'] . "</div>";
                    echo "<div class='row justify-content-center align-items-center'>" . $_SESSION['go_backBtn'] . "</div>";
                    echo "</div>";
                    unset($_SESSION['sent_message']);
                    unset($_SESSION['thnx_message']);
                    unset($_SESSION['getBack_message']);
                    unset($_SESSION['go_backBtn']);
                }

                if ($_SESSION['eror'] && $_SESSION['eror_message'] && $_SESSION['retry_message'] && $_SESSION['go_backBtn'] != null) {
                    echo "<div class='container' style='margin-top: 50px;'>";
                    echo "<div class='row mb-5 justify-content-center align-items-center'>" . $_SESSION['eror'] . "</div>";
                    echo "<div class='row mb-2 justify-content-center align-items-center'>" . $_SESSION['eror_message'] . "</div>";
                    echo "<div class='row mb-3 justify-content-center align-items-center'>" . $_SESSION['retry_message'] . "</div>";
                    echo "<div class='row justify-content-center align-items-center'>" . $_SESSION['go_backBtn'] . "</div>";
                    echo "</div>";
                    unset($_SESSION['eror']);
                    unset($_SESSION['eror_message']);
                    unset($_SESSION['retry_message']);
                    unset($_SESSION['go_backBtn']);
                }
                ?>
        </div>

    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>



</body>

</html>