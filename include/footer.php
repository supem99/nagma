<!-- Footer Section -->
<div class="col-md-12">
    <div class="row">
        <div class="social">
            <ul>
                <li><a href="https://www.facebook.com/profile.php?id=100013837939485"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/home"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.instagram.com/nagma_construction/"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.youtube.com/watch?v=6-C9ZeHC4oA"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div>

        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Web Created By Chandula Supem Perera<i class="fa fa-heart"></i> <br/>Contact = +94 71 5949 886</span>
                </div>
            </div>
         </footer>

    </div>
</div>


</div>
</div>
</body>
</html>
