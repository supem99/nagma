
<div id="portfolio">
    <div class="container">
        <div class="section-title">
            <h2>Our Works</h2>
        </div>

        <div class="row">
            <div class="portfolio-items">

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/56.jpg" title="New Home Construction" data-lightbox-gallery="gallery1">
                                <div class="hover-text">
                                    <h4>New Home Construction</h4>
                                </div>
                                <img src="img/new/1/56.jpg" class="img-responsive" alt="Project "> </a> </div>
                    </div>
                </div>


                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/54.jpg" title="New Home Construction" data-lightbox-gallery="gallery1">
                                <div class="hover-text">
                                    <h4>New Home Construction</h4>
                                </div>
                                <img src="img/new/1/54.jpg" class="img-responsive" alt="Project "> </a> </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/61.jpg" title="New Home Construction" data-lightbox-gallery="gallery1">
                                <div class="hover-text">
                                    <h4>New Home Construction</h4>
                                </div>
                                <img src="img/new/1/61.jpg" class="img-responsive" alt="Project "> </a> </div>
                    </div>
                </div>


                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/22.jpg" title="New Home Construction" data-lightbox-gallery="gallery1">
                                <div class="hover-text">
                                    <h4>New Home Construction</h4>
                                </div>
                                <img src="img/new/1/22.jpg" class="img-responsive" alt="Project "> </a> </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/27.jpg" title="New Home Construction" data-lightbox-gallery="gallery1">
                                <div class="hover-text">
                                    <h4>New Home Construction</h4>
                                </div>
                                <img src="img/new/1/27.jpg" class="img-responsive" alt="Project "> </a> </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="img/new/1/15.jpg" title="New Home Construction" data-lightbox-gallery="gallery1">
                                <div class="hover-text">
                                    <h4>New Home Construction</h4>
                                </div>
                                <img src="img/new/1/15.jpg" class="img-responsive" alt="Project "> </a> </div>
                    </div>
                </div>


                <center>   <a href="image.php"><button name="insert" class="btn btn-custom btn-lg">More Images </button></a>  </center>
    </div>
</div>
