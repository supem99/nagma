

<?php  include ('include/header.php'); ?>
<?php  include ('include/navbar.php'); ?>

<!-- Header -->
<header id="header">
    <div class="intro">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 intro-text">

                        <font size="100" color="#f0f8ff"> <b>නිවාස ඉදිකිරීම..<br>

                                හා<br>
                                 අලුත් වැඩියා කිරීම</b></font>
                        <h1><b><p> සාදරයෙන් පිළිගනිමු නැග්ම ඉදිකිරීම් සමාගමට </p></b></h1>

                    </div>
                </div>
            </div>
        </div>
</header>



<?php

include "imghome.php";

?>




<!-- Services Section -->
<div id="services">
    <div class="container">
        <div class="section-title">
            <h2>අපගේ සේවාවන්</h2>
        </div>

        <div class="about-text">
            <p><b> අපි සියලු උපකරණ යුරෝපීය රටවලින් ආනයනය කළ නමුත් පාරිභෝගිකයින්ගේ තේරිම් අනුව අපට ඒවා ආනයනය කළ හැකිය.
                    අපි නිවාස සැලසුම කරන අතර එහි 3D නිර්මාණය පාරිභෝගිකයාට ලබා දෙමු. එසේම අපි නව තාක්‍ෂණයෙන් උසස් ගුණාත්මක නිවාස සාදන්නෙමු.</b>
            </p>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-1.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>අලුතින් නිවාස ඉදිකිරිම</h3>

                </div>
            </div>
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-2.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>නිවාස අලුත් වැඩියාව</h3>

                </div>
            </div>
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-3.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>නාන කාමර අලුත් වැඩියාව</h3>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-4.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>මුළුතැන්ගෙය අලුත් වැඩියාව</h3>

                </div>
            </div>
            <div class="col-md-4">
                <div class="service-media"> <img src="img/services/service-5.jpg" alt=" "> </div>
                <div class="service-desc">
                    <h3>ජනෙල් සහ දොරවල්</h3>

                </div>
            </div>
            <div class="col-md-4">
                <div class="service-media"> <img src="http://cdn.home-designing.com/wp-content/uploads/2014/07/free-3-bedroom-house-plans.jpeg" alt=" "> </div>
                <div class="service-desc">
                    <h3>සැලසුම්</h3>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Testimonials Section -->

<!--<div id="testimonials">-->
<!--    <div class="container">-->
<!--        <div class="section-title">-->
<!--            <h2>Testimonials</h2>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col-md-4">-->
<!--                <div class="testimonial">-->
<!--                    <div class="testimonial-image"> <img src="img/testimonials/01.jpg" alt=""> </div>-->
<!--                    <div class="testimonial-content">-->
<!--                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."</p>-->
<!--                        <div class="testimonial-meta"> - John Doe </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <div class="testimonial">-->
<!--                    <div class="testimonial-image"> <img src="img/testimonials/02.jpg" alt=""> </div>-->
<!--                    <div class="testimonial-content">-->
<!--                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis."</p>-->
<!--                        <div class="testimonial-meta"> - Johnathan Doe </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <div class="testimonial">-->
<!--                    <div class="testimonial-image"> <img src="img/testimonials/03.jpg" alt=""> </div>-->
<!--                    <div class="testimonial-content">-->
<!--                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."</p>-->
<!--                        <div class="testimonial-meta"> - John Doe </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row"> </div>-->
<!--            <div class="col-md-4">-->
<!--                <div class="testimonial">-->
<!--                    <div class="testimonial-image"> <img src="img/testimonials/04.jpg" alt=""> </div>-->
<!--                    <div class="testimonial-content">-->
<!--                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."</p>-->
<!--                        <div class="testimonial-meta"> - Johnathan Doe </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <div class="testimonial">-->
<!--                    <div class="testimonial-image"> <img src="img/testimonials/05.jpg" alt=""> </div>-->
<!--                    <div class="testimonial-content">-->
<!--                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."</p>-->
<!--                        <div class="testimonial-meta"> - John Doe </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <div class="testimonial">-->
<!--                    <div class="testimonial-image"> <img src="img/testimonials/06.jpg" alt=""> </div>-->
<!--                    <div class="testimonial-content">-->
<!--                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis."</p>-->
<!--                        <div class="testimonial-meta"> - Johnathan Doe </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<!--about-->

<div id="about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <img src="img/img.jpg" class="img-responsive" alt="">
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="about-text">
                    <h2>අපි කවුද?</h2>
                    <p>

                        අපි නැග්ම ඉදිකිරීම් පුද්ගලික සමාගම. අපි නිවාස ඉදිකර අලුත්වැඩියා කරනු ලැබේ.
                        එසේම අපි ගෙවතු අලංකාර කිරීම , සියලු විදුලි වැඩ සහ නිවාස ආදිය සියලු වැඩ කරනෙමු. අපට අවුරුදු 25 ක පලපුරුදක් ඇත.
                        යුරෝපයේ අවුරුදු 15 ක් සහ  ලංකාවේ අවුරුදු 10 ක (ලන්ඩන්, නෙදර්ලන්තය, පැරිස්, ස්පාඤ්ඤය, බෙල්ජියම, ඉතාලිය) අත්දැකීම් අපට ඇත.
                        එසේම, ඉදිකිරීම් මාලාවේ සෑම අංශයකම සේවකයින් අප හා සේවය කරයි.

                    </p>
                    <h3>
                        අපව තෝරා ගන්නේ ඇයි?</h3>
                    <div class="list-style">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <ul>
                                <li>වසර ගණනාවක පළපුරුද්ද</li>
                                <li>සම්පූර්ණයෙන්ම රක්ෂණය කර ඇත</li>
                                <li>පිරිවැය කළමනාකරණය</li>
                                <li>100% තෘප්තිමත් සහතිකය</li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <ul>
                                <li>නොමිලේ උපදේශනය</li>
                                <li>තෘප්තිමත් සේවාවන්</li>
                                <li>වියාපෘති ළමනාකරණය</li>
                                <li>දැරිය හැකි සාදාරන මිලගණන්</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/about-->


<!-- Contact Section -->

<!--last-->

<div id="contact">
    <div class="container">
        <div class="col-md-8">
            <div class="row">
                <div class="section-title">
                    <h2>අප සමඟ සම්බන්ධ වීමට </h2>
                    <p>අපට සම්බන්ද කරගැනීමට කරුණාකර පහත පෝරමය පුරවන්න, හැකි ඉක්මනින් අපි ඔබ හා සම්බන්ද වන්නෙමු.</p>
                </div>

                <form novalidate action="contMail.php" method='post'>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="නම" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="contNum" placeholder="දුරකථන අංකය" maxlength="100" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="conMail" placeholder="Email ලිපිනය" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="conSubj" placeholder="මාතෘකාව" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" name="conMsg" rows="6"   placeholder="ඔබේ පණිවිඩය" required></textarea>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-custom btn-lg" type="submit">Send</button>

                </form>



            </div>
        </div>



        <div class="col-md-3 col-md-offset-1 contact-info">
            <div class="contact-item">
                <h4>තොරතුරු දැනගැනීමට</h4>
                <p><span>ලිපිනය</span>
                    No:583/ A ගාලුපාර<br>
                    වාද්දුව</p>
            </div>
            <div class="contact-item">
                <p><span>දුරකථන අංකය</span> +94 77 313 5839
                    <i class="fa fa-whatsapp"></i>
                </p>
                <p><span>Fax අංකය</span> +94 38 22 94 025
                </p>
            </div>
            <div class="contact-item">
                <p><span>Email ලිපිනය</span> construction.nagma@gmail.com</p>
            </div>
        </div>
        <!--last-->


        <?php  include ('include/footer.php'); ?>
        <?php  include ('include/script.php'); ?>


